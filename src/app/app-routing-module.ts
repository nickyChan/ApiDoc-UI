import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ContentComponent} from './front/content/content.component';

const appRoutes: Routes = [
  {path: 'guide/:id', component: ContentComponent},
  // {path: '', redirectTo: 'content', pathMatch: 'full'},
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}

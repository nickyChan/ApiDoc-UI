import {Component, Input} from '@angular/core';
import {Router} from "@angular/router";
import 'rxjs/add/operator/switch';

@Component({
  selector: 'app-tree-menu',
  templateUrl: './tree-menu.component.html',
  styleUrls: ['./tree-menu.component.css']
})
export class TreeMenuComponent{

  @Input()
  node;
  @Input()
  show=false;

  constructor(private router: Router) {
  }

  go() {
    if (!this.node.children)
      this.router.navigate(['content', this.node.id], {queryParams: {link: this.node.link}});
  }
}

import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  menus = [
    {
      name: '快速上手', id: 1
      // children: []
    },
    {
      name: '教程', id: 2,
      children: [
        {name: '简介', id: 21},
        {name: '英雄编辑器', id: 22},
        {name: '主从结构', id: 23},
        {name: '多个组建', id: 24},
        {name: '服务', id: 25, children: [{name: '服务A', id: 251}, {name: '服务B', id: 252}]},
        {name: '路由', id: 26},
        {name: 'HTTP', id: 27},
      ]
    },
    {
      name: 'API参考手册', id: 3
    }
  ];

  constructor() {
  }

  ngOnInit() {
  }

}

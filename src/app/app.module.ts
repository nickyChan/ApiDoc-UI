import {NgModule} from '@angular/core';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatExpansionModule,
  MatIconModule,
  MatListModule,
  MatSidenavModule,
  MatToolbarModule,
} from '@angular/material';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  CovalentExpansionPanelModule,
  CovalentJsonFormatterModule,
  CovalentLayoutModule,
  CovalentStepsModule
} from '@covalent/core';

import {AppComponent} from './app.component';
import {LayoutComponent} from './front/layout/layout.component';
import {TreeMenuComponent} from './front/tree-menu/tree-menu.component';
import {ContentComponent} from './front/content/content.component';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app-routing-module';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    TreeMenuComponent,
    LayoutComponent,
    ContentComponent,
  ],
  imports: [
    BrowserModule,
    CovalentLayoutModule,
    CovalentStepsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    RouterModule,
    AppRoutingModule,
    MatButtonModule, MatCheckboxModule,
    MatExpansionModule, MatIconModule,
    MatSidenavModule, MatToolbarModule,
    MatCardModule, MatListModule,

    CovalentExpansionPanelModule, CovalentJsonFormatterModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
